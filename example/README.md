# Usage Example

This is an example on how to use the plugins.

__You will need an account on Mahisoft's Nexus repository.__

1. Clone this repository

2. Copy the contents of the `your_home_gradle` directory to `~/.gradle`

3. Update the file `~/.gradle/gradle.properties` and set your Mahisoft's Nexus username and password

4. On this repository directory, run `./gradlew properties`. The kamino plugin should show on the property list

5. This example runs the Kamino common plugin, which is only code styling and java options:

   `id 'com.mahisoft.kamino.common' version '1.0.0-14'`

6. You can run `./gradlew assemble` to compile and generate a jar file with the test class.

You don't need to do steps 1-2 after the first time. These settings will be shared
among all projects that use Kamino plugins.
