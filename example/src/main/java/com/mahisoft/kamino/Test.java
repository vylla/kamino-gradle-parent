package com.mahisoft.kamino;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
final class Test {

    private Test() {}

    public static void main(String[] args) {
        SpringApplication.run(Test.class, args);
    }
}