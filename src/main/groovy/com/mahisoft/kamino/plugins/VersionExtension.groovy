package com.mahisoft.kamino.plugins

import org.gradle.api.Project

class VersionExtension {
    Integer major = null
    Integer minor = null
    Integer patch = null
    private Project project

    VersionExtension(Project project, boolean setDefault) {
        this.project = project
        if (setDefault) {
            major = 1
            minor = 0
        }
    }

    def updateProjectVersion(Project override = null) {
        Project theProject = override ?: project

        if (major != null && minor != null) {

            def release = System.getenv('RELEASE')
            def isRelease = release != null && release == 'true'
            Integer patch
            boolean isDev = false
            if (this.patch != null) {
                patch = this.patch
            } else {
                patch = Optional.ofNullable(System.getenv('BUILD_NUMBER')).map { it.toInteger() }.orElse(null)
            }
            if (patch == null) {
                patch = 0
                isDev = true
            }
            theProject.version = "${this.major}.${this.minor}.$patch" + (isDev ? '-DEV' : '') + (isRelease ? '' : '-SNAPSHOT')
        }

    }
}
