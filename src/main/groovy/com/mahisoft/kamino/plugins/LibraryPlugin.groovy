package com.mahisoft.kamino.plugins

import org.gradle.api.Project
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin

class LibraryPlugin extends CommonPlugin {

    @Override
    void apply(Project project) {

        super.apply(project)

        project.plugins.apply(MavenPublishPlugin.class)
        sharedRepoExtension.configureUploadRepo()

        //This forces to run check (test/coverage/pmd) before publishing. Publish to local maven repo, building a jar does not enforce quality checks.
        def publishTask = project.tasks.getByName('publish')
        if (publishTask != null) {
            publishTask.dependsOn(project.tasks.getByName('check'))
        }



        //This overrides snapshot updates
        project.configurations.all {
            it.resolutionStrategy.cacheChangingModulesFor 0, 'seconds'
        }
    }
}