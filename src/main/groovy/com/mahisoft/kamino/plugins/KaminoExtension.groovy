package com.mahisoft.kamino.plugins

import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.DependencyHandler

class KaminoExtension {
    KaminoExtension(Project project) {

    }

    void objectMapper(DependencyHandler dependencyHandler) {
        def jacksonVersion = '2.9.1'

        dependencyHandler.add('compile', "com.fasterxml.jackson.core:jackson-databind:$jacksonVersion")
        dependencyHandler.add('compile', "com.fasterxml.jackson.core:jackson-annotations:$jacksonVersion")
        dependencyHandler.add('compile', "com.fasterxml.jackson.datatype:jackson-datatype-jsr310:$jacksonVersion")
    }
}
