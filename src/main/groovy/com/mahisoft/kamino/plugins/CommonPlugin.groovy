package com.mahisoft.kamino.plugins

import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.plugins.quality.PmdExtension
import org.gradle.api.plugins.quality.PmdPlugin
import org.gradle.testing.jacoco.plugins.JacocoPlugin
import org.gradle.testing.jacoco.plugins.JacocoPluginExtension

class CommonPlugin implements Plugin<Project> {

    protected SharedRepoExtension sharedRepoExtension

    @Override
    void apply(Project project) {
        // Plugins
        project.plugins.apply("io.franzbecker.gradle-lombok")
        project.plugins.apply(JavaPlugin.class)
        project.plugins.apply(PmdPlugin.class)
        project.plugins.apply(JacocoPlugin.class)

        def versionExtension = project.extensions.create('versionInfo', VersionExtension, project, false)
        sharedRepoExtension = project.extensions.create('sharedRepo', SharedRepoExtension, project, true)

        sharedRepoExtension.configureDownloadRepo()

        //Java Configuration
        def javaConfig = project.convention.plugins.get('java') as JavaPluginConvention
        javaConfig.sourceCompatibility = JavaVersion.VERSION_1_8
        javaConfig.targetCompatibility = JavaVersion.VERSION_1_8

        //PMD Configuration
        def pmdConfig = project.extensions.getByType(PmdExtension.class)
        pmdConfig.toolVersion = '5.8.1'
        def scanner = new Scanner(getClass().getResourceAsStream("/pmdrules.xml")).useDelimiter("\\A")
        if (scanner.hasNext()) {
            pmdConfig.ruleSetConfig = project.resources.text.fromString(scanner.next())
        } else {
            project.logger.error("Could not load PMD ruleset.")
        }

        //Jacoco configuration
        def jacocoConfig = project.extensions.getByType(JacocoPluginExtension.class)
        jacocoConfig.toolVersion = '0.8.0'
        def reportTask = project.tasks.getByName('jacocoTestReport')
        project.configure(reportTask) {
            reports {
                xml.enabled true
            }
        }

        //Base Repositories
        project.repositories {
            mavenLocal()
            mavenCentral()
            jcenter()
        }

        def verificationTask = project.tasks.getByName('jacocoTestCoverageVerification')
        project.configure(verificationTask) {
            violationRules {
                rule {
                    limit {
                        counter = 'LINE'
                        minimum = 0.8
                    }
                }

                rule {
                    limit {
                        counter = 'BRANCH'
                        minimum = 0.6
                    }
                }
            }
        }
        project.tasks.getByName('check').dependsOn(verificationTask)
        verificationTask.dependsOn(reportTask)

        project.dependencies {
            add('compile', 'com.google.guava:guava:23.0')
            add('compile', 'org.apache.commons:commons-lang3:3.6')
        }

        project.afterEvaluate {
            versionExtension.updateProjectVersion()
        }
    }
}
