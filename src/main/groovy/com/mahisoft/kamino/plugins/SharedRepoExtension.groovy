package com.mahisoft.kamino.plugins

import org.gradle.api.Project
import org.gradle.api.credentials.PasswordCredentials
import org.gradle.api.logging.LogLevel
import org.gradle.api.plugins.ExtraPropertiesExtension
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication

class SharedRepoExtension {
    public static final String LEGACY_NEXUS_USERNAME = 'mahiNexusUsername'
    public static final String LEGACY_NEXUS_PASSWORD = 'mahiNexusPassword'
    public static final String LEGACY_NEXUS_REPOSITORY = 'kaminoRepository'
    public static final String LEGACY_NEXUS_SNAPSHOT = "kaminoSnapshotRepository"
    public static final String LEGACY_NEXUS_RELEASE = "kaminoReleaseRepository"


    public static final String REPO_USERNAME = 'sharedMavenUser'
    public static final String REPO_PASSWORD = 'sharedMavenPassword'
    public static final String REPO_URL = 'sharedMavenUrl'
    public static final String REPO_SNAPSHOT = 'snapshotUploadUrl'
    public static final String REPO_RELEASE = 'releaseUploadUrl'

    String url = null
    String username = null
    String password = null
    String snapshotUploadUrl = null
    String releaseUploadUrl = null

    private Project project
    private boolean higherPriority

    SharedRepoExtension(Project project, boolean higherPriority = false) {
        this.project = project
        this.higherPriority = higherPriority
    }

    static private def getRepoOrNull(Project project, String name) {
        try {
            return project.repositories.getByName(name)
        }
        catch (Exception e) {
            return null
        }
    }

    def configureDownloadRepo(Project override = null) {
        final Project theProject = override ?: project
        final Project configProject = project
        if (higherPriority) {
            def repo = getRepoOrNull(theProject, 'centralized')
            if (repo != null) {
                theProject.repositories.remove(repo)
            }
        }

        if (getRepoOrNull(theProject, 'centralized') == null) {
            theProject.repositories {
                // Common Maven repository
                it.maven { repo ->
                    repo.name = 'centralized'
                    repo.credentials {
                        username = getSharedUsername(configProject, this)
                        password = getSharedPassword(configProject, this)
                    } as PasswordCredentials

                    repo.url = getSharedMavenUrl(configProject, this)
                }
            }
        }
    }


    def configureUploadRepo(Project override = null) {
        final Project theProject = override ?: project
        final Project configProject = project
        def snapshotRepo = getSharedSnapshotUrl(configProject, this)
        def releaseRepo = getSharedReleaseUrl(configProject, this)
        def release = System.getenv('RELEASE')
        def isRelease = release != null && release == 'true'

        final def publishRepository = isRelease ? releaseRepo : snapshotRepo

        if (higherPriority) {
            def repo = getRepoOrNull(theProject, 'centralizedUpload')
            if (repo != null) {
                theProject.repositories.remove(repo)
            }
        }

        boolean addRepo = getRepoOrNull(theProject, 'centralizedUpload') == null

        try {
            theProject.extensions.configure(PublishingExtension.class) {
                if (addRepo) {
                    it.repositories.maven {
                        name 'centralizedUpload'
                        url publishRepository
                        credentials {
                            username getSharedUsername(configProject, this)
                            password getSharedPassword(configProject, this)
                        }
                    }
                }

                it.publications.configure {
                    mavenJava(MavenPublication) {
                        from theProject.components.getByName('java')
                    }
                }
            }
        }
        catch (Exception e) {
            theProject.logger.log(LogLevel.INFO, 'Not configuring Publishing extension since not present.')
        }
    }

    private static String getOrNull(final ExtraPropertiesExtension self, String key) {
        try {
            return self.get(key)
        }
        catch (ExtraPropertiesExtension.UnknownPropertyException e) {
            return null
        }
    }

    private
    static String getSharedDatum(Project project, String extensionValue, String currentKey, String legacyKey, String defaultValue = null) {
        // Precedence order is:
        // 1. Extension value
        // 1a. Gradle property
        // 2. New value names (sharedX)
        // 3. Legacy value names (mahiNexusX)
        if (extensionValue != null)
            return extensionValue

        String v0 = project.getGradle().getProperties().get(currentKey)
        if(v0 != null)
            return v0

        String v1 = getOrNull(project.extensions.getExtraProperties(), currentKey)
        if (v1 != null)
            return v1

        String v2 = getOrNull(project.extensions.getExtraProperties(), legacyKey)
        if (v2 != null)
            return v2

        return defaultValue
    }

    static String getSharedUsername(Project project, SharedRepoExtension extension) {
        return getSharedDatum(project, extension.username, REPO_USERNAME, LEGACY_NEXUS_USERNAME)
    }

    static String getSharedPassword(Project project, SharedRepoExtension extension) {
        return getSharedDatum(project, extension.password, REPO_PASSWORD, LEGACY_NEXUS_PASSWORD)
    }

    static String getSharedMavenUrl(Project project, SharedRepoExtension extension) {
        return getSharedDatum(project, extension.url, REPO_URL, LEGACY_NEXUS_REPOSITORY, 'https://nexus.mahisoft.com/repository/maven-public/')
    }

    static String getSharedSnapshotUrl(Project project, SharedRepoExtension extension) {
        return getSharedDatum(project, extension.snapshotUploadUrl, REPO_SNAPSHOT, LEGACY_NEXUS_SNAPSHOT, 'https://nexus.mahisoft.com/repository/maven-snapshots/')
    }

    static String getSharedReleaseUrl(Project project, SharedRepoExtension extension) {
        return getSharedDatum(project, extension.releaseUploadUrl, REPO_RELEASE, LEGACY_NEXUS_RELEASE, 'https://nexus.mahisoft.com/repository/maven-releases/')
    }
}
