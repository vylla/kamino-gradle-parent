package com.mahisoft.kamino.plugins

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin

class ParentPlugin implements Plugin<Project> {

    private VersionExtension versionExtension
    private SharedRepoExtension globalRepoExtension

    @Override
    void apply(Project project) {
        versionExtension = project.extensions.create('versionInfo', VersionExtension, project, true)
        globalRepoExtension = project.extensions.create('sharedRepo', SharedRepoExtension, project, false)

        project.subprojects {
            globalRepoExtension.configureDownloadRepo(it)
            globalRepoExtension.configureUploadRepo(it)

            afterEvaluate {
                versionExtension.updateProjectVersion(it)
            }
        }
    }
}
