package com.mahisoft.kamino.plugins

import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension
import org.gradle.api.Project
import org.gradle.api.plugins.ApplicationPlugin
import org.gradle.api.tasks.SourceSetContainer
import org.springframework.boot.gradle.run.BootRunTask
import se.transmode.gradle.plugins.docker.DockerPluginExtension

class MicroservicePlugin extends CommonPlugin {

    @Override
    void apply(Project project) {
        super.apply(project)

        project.plugins.apply("org.springframework.boot")
        project.plugins.apply(ApplicationPlugin.class)
        project.plugins.apply('docker')

        def dockerExtension = project.extensions.getByType(DockerPluginExtension.class)

        dockerExtension.baseImage = System.getenv('DOCKER_BASE_IMAGE') ?: dockerExtension.baseImage ?: 'azul/zulu-openjdk-alpine:8'

        dockerExtension.maintainer = System.getenv('DOCKER_MAINTAINER') ?: dockerExtension.maintainer ?: 'Mahisoft Inc. "info@mahisoft.com"'

        //This forces quality checks prior to building a docker image.
        def task = project.tasks.getByName('distDocker').dependsOn(project.tasks.getByName('check'))

        def tag = System.getenv('DOCKER_TAG')

        if (tag != null) {
            task.setProperty("tag", tag)
        }

        def sourceSets = project.getProperties().get('sourceSets') as SourceSetContainer
        sourceSets.configure {
            dev {
                resources {
                    srcDir 'src/dev/resources'
                }
                compileClasspath += sourceSets.getByName('main').runtimeClasspath
            }
        }

        def runTask = project.tasks.getByName('bootRun') as BootRunTask
        runTask.classpath(sourceSets.getByName('dev').runtimeClasspath)


        //This overrides snapshot update policy
        def springExtension = project.extensions.findByType(DependencyManagementExtension.class)
        springExtension.resolutionStrategy {
            cacheChangingModulesFor 0, 'seconds'
        }
    }
}
