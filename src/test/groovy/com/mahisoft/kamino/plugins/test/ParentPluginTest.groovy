package com.mahisoft.kamino.plugins.test

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class ParentPluginTest extends Specification {

    @Rule
    final TemporaryFolder testProjectDir = new TemporaryFolder()
    File buildFile
    File contractFile
    File serviceFile
    File settingsFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
        testProjectDir.newFolder("contract")
        testProjectDir.newFolder("service")
        contractFile = testProjectDir.newFile('contract/build.gradle')
        serviceFile = testProjectDir.newFile('service/build.gradle')
        settingsFile = testProjectDir.newFile('settings.gradle')
    }

    def "common plugin loads properly"() {
        given:
        buildFile << """        
            plugins {
                id 'com.mahisoft.kamino.parent'
            }
        """

        contractFile << """
            apply plugin: 'com.mahisoft.kamino.library'
        """

        serviceFile << """
            apply plugin: 'com.mahisoft.kamino.microservice'
        """

        settingsFile << """
            include "contract"
            include "service"
        """

        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments('properties')
                .withPluginClasspath()
                .build()

        then:
        println(result.output)
        true
    }
}