package com.mahisoft.kamino.plugins.test

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class CommonPluginTest extends Specification {

    @Rule final TemporaryFolder testProjectDir = new TemporaryFolder()
    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
    }

    def "common plugin loads properly"() {
        given:
        buildFile << """        
            plugins {
                id 'com.mahisoft.kamino.common'
            }
        """

        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments('properties')
                .withPluginClasspath()
                .build()

        then:
            println(result.output)
            true
    }
}
