# Kamino Gradle Plugin

This gradle plugin can be used to quickly and easily integrate a library
or project with the Kamino ecosystem.

It can be used for two types of projects:
 -  Libraries: A library project that will produce a jar artifact, and will be
    uploaded to a maven repository.
    
 -  Microservices: A project that will produce a Docker image
    that can be uploaded to a Docker registry.
    
## Usage

See [example](example/README.md) for setup instructions for your environment.

To enable the microservice plugin:
```
plugins {
    id 'com.mahisoft.kamino.microservice' version '1.0.0-<latestVersion>'
}
```

To enable the library plugin:
```
plugins {
    id 'com.mahisoft.kamino.library' version '1.0.0-<latestVersion>'
}
```

## Configuration

You can configure the following properties on your `~/.gradle/gradle.properties`
file to change the behavior of the plugin:

 - `mahiNexusUsername`: Username to use when logging in to the maven repository
 - `mahiNexusPassword`: The password to use when logging in to the maven repository
 - `kaminoRepository`: The maven repository to use. Defaults to `https://nexus.mahisoft.com/repository/maven-public/`
 - `kaminoSnapshotRepository` (Library plugin only): The upload destination of snapshot artifacts.
    Defaults to `https://nexus.mahisoft.com/repository/maven-snapshots/`
 - `kaminoReleaseRepository` (Library plugin only): The upload destination of release artifacts.
    Defaults to `https://nexus.mahisoft.com/repository/maven-releases/`